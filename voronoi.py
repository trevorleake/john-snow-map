from itertools import product
from scipy.spatial import Voronoi
from PIL import Image, ImageDraw

def inTriangle(p, (a, b, c)):
	def det(x, y, z):
		return (x[0]-z[0]) * (y[1]-z[1]) - (y[0]-z[0]) * (x[1]-z[1])

	s = det(p, a, b) < 0
	t = det(p, b, c) < 0
	r = det(p, c, a) < 0
	return (s == t) and (t == r)

def neighborhood(x, y):
	return [
		(x-1, y-1),
		(x-1, y-0),
		(x-1, y+1),
		(x-0, y-1),
		(x-0, y-0),
		(x-0, y+1),
		(x+1, y-1),
		(x+1, y-0),
		(x+1, y+1)
	]


image = Image.open("jon-snow-map.jpg").convert("RGBA")

coords = []
f = open("coords.txt", "r")
for line in f:
	l = line.strip()
	coords.append((int(l[:l.index(", ")]), int(l[l.index(", ")+2:])))
pix = image.load()

size_factor = 2.5
coords = [tuple((size_factor*x, size_factor*y)) for (x, y) in coords]


regions = []
indices = Voronoi(coords).regions
verts = Voronoi(coords).vertices
for inds in indices:
	if len(inds) > 2 and -1 not in inds:
		c = [tuple(verts[n].tolist()) for n in inds]
		regions.append(c)



im = Image.new("RGBA", image.size, (255,255,255,0))
draw = ImageDraw.Draw(im)
n = 0
for region in regions:
	draw.polygon(region, fill=((20*n) %255, (60*n) %255, (100*n) %255, 50))
	n += 1 
image = Image.alpha_composite(image, im)

print Voronoi(coords).ridge_points

image.show()
exit()

def line((x1, y1), (x2, y2), x):
	return (y2-y1)*x/(x2-x1) + y1 - (y2-y1)*x1/(x2-x1)




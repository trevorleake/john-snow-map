import numpy as np
import matplotlib.pyplot as plt
from scipy.spatial import Voronoi
from PIL import Image, ImageDraw

def voronoi_finite_polygons_2d(vor, radius=None):
    """
    Reconstruct infinite voronoi regions in a 2D diagram to finite
    regions.

    Parameters
    ----------
    vor : Voronoi
        Input diagram
    radius : float, optional
        Distance to 'points at infinity'.

    Returns
    -------
    regions : list of tuples
        Indices of vertices in each revised Voronoi regions.
    vertices : list of tuples
        Coordinates for revised Voronoi vertices. Same as coordinates
        of input vertices, with 'points at infinity' appended to the
        end.

    """

    if vor.points.shape[1] != 2:
        raise ValueError("Requires 2D input")

    new_regions = []
    new_vertices = vor.vertices.tolist()

    center = vor.points.mean(axis=0)
    if radius is None:
        radius = vor.points.ptp().max()

    # Construct a map containing all ridges for a given point
    all_ridges = {}
    for (p1, p2), (v1, v2) in zip(vor.ridge_points, vor.ridge_vertices):
        all_ridges.setdefault(p1, []).append((p2, v1, v2))
        all_ridges.setdefault(p2, []).append((p1, v1, v2))

    # Reconstruct infinite regions
    for p1, region in enumerate(vor.point_region):
        vertices = vor.regions[region]

        if all(v >= 0 for v in vertices):
            # finite region
            new_regions.append(vertices)
            continue

        # reconstruct a non-finite region
        ridges = all_ridges[p1]
        new_region = [v for v in vertices if v >= 0]

        for p2, v1, v2 in ridges:
            if v2 < 0:
                v1, v2 = v2, v1
            if v1 >= 0:
                # finite ridge: already in the region
                continue

            # Compute the missing endpoint of an infinite ridge

            t = vor.points[p2] - vor.points[p1] # tangent
            t /= np.linalg.norm(t)
            n = np.array([-t[1], t[0]])  # normal

            midpoint = vor.points[[p1, p2]].mean(axis=0)
            direction = np.sign(np.dot(midpoint - center, n)) * n
            far_point = vor.vertices[v2] + direction * radius

            new_region.append(len(new_vertices))
            new_vertices.append(far_point.tolist())

        # sort region counterclockwise
        vs = np.asarray([new_vertices[v] for v in new_region])
        c = vs.mean(axis=0)
        angles = np.arctan2(vs[:,1] - c[1], vs[:,0] - c[0])
        new_region = np.array(new_region)[np.argsort(angles)]

        # finish
        new_regions.append(new_region.tolist())

    return new_regions, np.asarray(new_vertices)

def neighborhood(x, y):
	return [
		(x-1, y-1),
		(x-1, y-0),
		(x-1, y+1),
		(x-0, y-1),
		(x-0, y-0),
		(x-0, y+1),
		(x+1, y-1),
		(x+1, y-0),
		(x+1, y+1)
	]



image = Image.open("john-snow-map.jpg").convert("RGBA")
coords = []
f = open("coords.txt", "r")
for line in f:
	l = line.strip()
	coords.append((int(l[:l.index(", ")]), int(l[l.index(", ")+2:])))
pix = image.load()

size_factor = 2.5
coords = [tuple((size_factor*x, size_factor*y)) for (x, y) in coords]

# compute Voronoi tesselation
vor = Voronoi(coords)

# plot
regions, vertices = voronoi_finite_polygons_2d(vor)
regions = [[tuple(vertices[n].tolist()) for n in region] for region in regions]

colorset = [
    (230, 25, 75),  #
    (60, 180, 75),  #
    (255, 225, 25), #
    (0, 60, 200),   #
    (185, 20, 248), #
    (20, 200, 20),  #

    (70, 240, 240),
    (240, 50, 230),

    (255, 200, 10), #

    (0, 180, 50),
    (20, 20, 255),
    (255,0,200),
    (0, 255, 255)
]


opacity = 100

im = Image.new("RGBA", image.size, (255,255,255,0))
draw = ImageDraw.Draw(im)
n = 0
for region in regions:
    draw.polygon(region, fill=tuple(list(colorset[n]) + [opacity]))
#    draw.polygon(region, fill=(220*n%255, 40*n%255, 120*(2+n)%255, opacity))
    n += 1

#pix = im.load()
#for (x, y) in coords:
#    for (a,b) in neighborhood(x, y):
#        pix[a,b] = (255,0,0)

image = Image.alpha_composite(image, im)
image.save("voronoi.jpg")

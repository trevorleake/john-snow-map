from PIL import Image, ImageDraw, ImageTk
import Tkinter




window = Tkinter.Tk(className="pic")

image = Image.open("jon-snow-map.jpg")
image = image.resize((int(image.size[0]/2.5), int(image.size[1]/2.5)), Image.ANTIALIAS)
canvas = Tkinter.Canvas(window, width=image.size[0], height=image.size[1])
canvas.pack()
image_tk = ImageTk.PhotoImage(image)

canvas.create_image(image.size[0]//2, image.size[1]//2, image=image_tk)

def callback(event):
	print "clicked at: ", event.x, event.y
	f = open("coords.txt", "a")
	f.write(str(event.x) + ", " + str(event.y) + "\n")
	f.close()

canvas.bind("<Button-1>", callback)
Tkinter.mainloop()


